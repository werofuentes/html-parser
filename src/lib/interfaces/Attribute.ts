export default interface Attribute {
  /**
   * Name of the attribute
   * Example: class
   */
  name: string;

  /**
   * Value for the attribute
   * Example: "btn"
   */
  value?: string;
};
