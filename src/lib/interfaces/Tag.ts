export default interface Tag {
  /**
   * HTML Tag's name
   */
  tagName: string;

  /**
   * Whether the tag needs a closing tag
   */
  endOfTagRequired: boolean;
};
