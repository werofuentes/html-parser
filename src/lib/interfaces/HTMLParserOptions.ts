export default interface HTMLParserOptions {
  /**
   * Whether the parser will print the tree's
   * construction.
   */
  printConstruction: boolean;
};
