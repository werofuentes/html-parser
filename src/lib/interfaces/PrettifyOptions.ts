export default interface PrettifyOptions {
  /**
   * Number of 'spaces' or 'tabs' to insert.
   */
  spaceWitdh?: number;

  /**
   * Option to check whether the output will use
   * tabs(\t) or spaces(\s)
   */
  useTabs?: boolean;
};
