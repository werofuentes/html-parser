export default class State {
  /**
   * HTML
   */
  protected static html: string;

  public static isEmpty() {
    return this.html.length === 0;
  }

  public static getText(): string {
    return this.html;
  }

  public static setText(html: string): void {
    this.html = html;
  }
}
