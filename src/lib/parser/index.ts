import Tag from '../interfaces/Tag';
import TagList from '../tags';
import { TAG, END_TAG, ATTRIBUTE } from '../regex';
import HTMLNode from '../HTMLNode';
import HTMLParser from '../HTMLParser';
import State from '../State';

const tags = `(${TagList.map((tag) => tag.tagName).join('|')})`;

export enum TokenType {
  closingTag = 0,
  openingTag,
  text
}

export interface Token {
  /**
   * Token's type such as the beginning of the tag, closing or text
   */
  type: TokenType;

  /**
   * Text extracted from the html content that will be process
   * to fill a HTMLTag
   */
  text: string;

  /**
   * Data from the tag
   */
  tag: Tag;

  /**
   * Point where the string will be slice
   */
  at: number;
}

export const getToken = (str: string): Token => {
  const tok = {
    type: -1,
    at: -1
  } as Token;

  const openingTags = new RegExp('^' + TAG.replace('$tag$', tags), 'g');

  if (openingTags.test(str)) {
    tok.type = TokenType.openingTag;
    openingTags.lastIndex = 0;
    const res = openingTags.exec(str);
    State.setText(State.getText().slice(openingTags.lastIndex));
    tok.text = res.shift();
    const tag = res.shift();
    tok.tag = TagList.find((t) => t.tagName === tag);
    return tok;
  }

  const closingTags = new RegExp('^' + END_TAG.replace('$tag$', tags), 'g');
  if (closingTags.test(str)) {
    tok.type = TokenType.closingTag;
    closingTags.lastIndex = 0;
    const res = closingTags.exec(str);
    State.setText(State.getText().slice(closingTags.lastIndex));
    tok.text = res.shift();
    const tag = res.shift();
    tok.tag = TagList.find((t) => t.tagName === tag);
    return tok;
  }

  if (tok.type === -1) {
    tok.text = '';

    while (!isTag(str) && str.length > 0) {
      tok.text += str.substring(0, 1);
      str = str.substring(1);
    }

    tok.at = tok.text.length;
    State.setText(State.getText().slice(tok.at));
    tok.text = str.substring(0, tok.at);
  }

  return tok;
};

export const isTag = (str: string): boolean => {
  const openingTags = new RegExp('^' + TAG.replace('$tag$', tags), 'g');
  if (openingTags.test(str)) return true;

  const closingTags = new RegExp('^' + END_TAG.replace('$tag$', tags), 'g');
  if (closingTags.test(str)) return true;

  return false;
};

export const parse = (text: string, tag: Tag, node: HTMLNode): void => {
  node.tagName = tag.tagName;
  text = text.replace(`<${tag.tagName}`, '');
  node.endOfTagFound = !tag.endOfTagRequired;

  const reg = new RegExp(ATTRIBUTE, 'g');

  let arr: Array<string> = [];
  while ((arr = reg.exec(text)) !== null) {
    let attr = trim(arr[0]).split('=');
    const identifer = attr.shift();
    switch (identifer) {
      case 'name':
        node.name = attr.length > 0 ? attr.shift() : undefined;
        break;
      case 'id':
        node.id = attr.length > 0 ? attr.shift() : undefined;
        break;
      case 'class':
        node.class = attr.length > 0 ? attr.shift().replace(/\s+/, ' ').split(/\s/g) : [];
        break;
      default:
        node.attributes.push({
          name: identifer,
          value: attr.length > 0 ? attr.shift() : undefined
        });
    }
  }
};

export const trim = (str: string): string => str.replace(/^\s+/, '').replace(/\s+$/, '');
