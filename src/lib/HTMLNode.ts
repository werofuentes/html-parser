import HTMLTag from './HTMLTag';
import HTMLParser from './HTMLParser';
import { getToken, TokenType, parse } from './parser';
import State from './State';

export default class HTMLNode extends HTMLTag {
  /**
   * Parent Node from this.
   */
  parent: HTMLNode;

  /**
   * All childrens from this.
   */
  children: Array<HTMLNode>;

  /**
   * Previous(left) sibling from this.
   */
  previousSibling: HTMLNode;

  /**
   * Next(right) sibling from this.
   */
  nextSibling: HTMLNode;

  /**
   * To know if the tag has the end tag found
   */
  endOfTagFound: boolean;

  /**
   * Position where the tag will be place in case is
   * inside other tag with text.
   */
  at: number;

  /**
   * Depth from this node in the tree.
   */
  depth: number;

  /**
   * Initialize the Node and process a part of text
   * and gets all of the fields such as class, id, 
   * tag name, etc.
   * 
   * @param parent Parent from this Node
   */
  constructor(parent: HTMLNode, depth: number = 0) {
    super();

    /**
     * Initialize properties
     */
    this.parent = parent;
    this.children = [];
    this.nextSibling = undefined;
    this.previousSibling = undefined;
    this.depth = depth;

    this.endOfTagFound = false;
    this.at = 0;

    /**
     * Set previousSibling and nextSibling
     */
    if (this.parent) {
      const { length } = this.parent.children;
      if (length > 0) {
        this.previousSibling = this.parent.children[length - 1];
        this.parent.children[length - 1].nextSibling = this;
      }
    }

    /**
     * Process text in a recursive way.
     * 
     * Cases:
     * 1. If we find a tag and 'this.tagName' is not fill(length equal to 0)
     * then in base in the token's text we parse it and set it to 'this'.
     * 
     * 2. If we find a tag and 'this.tagName' is fill(length greater than 0)
     * then we create a new HTMLNode and we add it to the children from 'this'
     * 
     * 3. If we find a closing tag and 'tagName' from tok is equal to 'this.tagName',
     * we finish and we change the state from 'this.endOfTagFound' to true so we go
     * out of the while.
     * 
     * 4. If we don't find a tag or closing tag and the 'this.endOfTagFound' it's 
     * false, that means is just text and we push to 'this.text'
     */
    while (!this.endOfTagFound) {
      const tok = getToken(State.getText());
      switch (tok.type) {
        case TokenType.openingTag:
          if (this.tagName.length > 0) {
            State.setText(`${tok.text}${State.getText()}`);
            const child = new HTMLNode(this, depth + 1);
            this.children.push(child);
          } else {
            parse(tok.text, tok.tag, this);
            if (this.parent) this.at = this.parent.text.length;
          }
          break;
        case TokenType.closingTag:
          if (tok.tag.tagName === this.tagName) {
            this.endOfTagFound = true;
          } else throw 'Invalid HTML Format';
          break;
        case TokenType.text:
          this.text.push(tok.text);
          break;
      }
    }
  }
}
