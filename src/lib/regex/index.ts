export const ATTRIBUTE = '\\s+[a-zA-Z\\-\\_]+[a-zA-Z\\d\\-\\_]+(=("|\')[^"]*("|\'))?';
export const TAG = '<$tag$(' + ATTRIBUTE + ')*\\s*/?\\s*>';
export const END_TAG = '<\\s*\\/?\\s*$tag$\\s*>';
export const DOCTYPE = /<!DOCTYPE html>/;
