import HTMLNode from './HTMLNode';
import HTMLParserOptions from './interfaces/HTMLParserOptions';
import State from './State';

export default class HTMLParser {
  /**
   * Root Node from the tree
   */
  root: HTMLNode;

  /**
   * Option to print out the tree construction 
   * to console
   */
  printConstruction: boolean;

  /**
   * 
   * @param text The HTML content validated
   * @param options Options to print out the
   * construction tree
   */
  constructor(options?: HTMLParserOptions) {
    if (options) this.printConstruction = options.printConstruction;
    else this.printConstruction = false;

    if (!State.isEmpty()) this.root = new HTMLNode(undefined);
  }
}
