import Attribute from './interfaces/Attribute';

export default class HTMLTag {
  /**
   * HTML Tag's name
   */
  tagName: string;

  /**
   * Attributes from the Tag that are not id, name or class.
   */
  attributes: Array<Attribute>;

  /**
   * HTML Text from Tag
   */
  text: Array<string>;

  /**
   * Tag's ID
   */
  id: string;

  /**
   * Tag's Name
   */
  name: string;

  /**
   * Tag's classes
   */
  class: string[];

  constructor() {
    this.tagName = '';
    this.attributes = [];
    this.text = [];
    this.id = '';
    this.name = '';
    this.class = [];
  }
}
