import Tag from './interfaces/Tag';
import TagList from './tags';
import { ATTRIBUTE, DOCTYPE } from './regex';
import State from './State';

export default class HTMLSanitizer {
  /**
   * HTML Content 
   */
  text: string;

  constructor(str: string) {
    State.setText(str.split(/\r?\n/).map((line) => line.replace(/^[\s\t]*/g, '')).join('').replace(DOCTYPE, ''));
  }
}
