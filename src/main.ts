import fs from 'fs';
import HTMLSanitizer from './lib/HTMLSanitizer';
import HTMLParser from './lib/HTMLParser';

(() => {
  const content = fs.readFileSync('index.html', { encoding: 'utf8' });
  const html = new HTMLSanitizer(content);
  const parser = new HTMLParser({ printConstruction: true });
  console.log(parser.root);
})();
